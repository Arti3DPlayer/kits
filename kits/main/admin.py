from django.contrib import admin
from kits.main.models import Feedback, MainPage, FaqPage, SpecialOfferPage, SpecialOfferItem, ConstactPage, FaqItem, FeelTheDifference, WhyItSoPopular, AboutPage, HowToInstallPage, HowToInstallVideo, HowToInstallActions

class WhyItSoPopularInline(admin.TabularInline):
    model = WhyItSoPopular
    extra = 1

class FeelTheDifferenceInline(admin.TabularInline):
    model = FeelTheDifference
    extra = 1

class HowToInstallActionsInline(admin.TabularInline):
    model = HowToInstallActions
    extra = 1

class HowToInstallVideoInline(admin.TabularInline):
    model = HowToInstallVideo
    extra = 1

class FaqItemInline(admin.TabularInline):
    model = FaqItem
    extra = 1

class SpecialOfferPageItemInline(admin.TabularInline):
    model = SpecialOfferItem
    extra = 1

class SpecialOfferPageAdmin(admin.ModelAdmin):
    inlines = [SpecialOfferPageItemInline,]

class ConstactPageAdmin(admin.ModelAdmin):
	list_display = ('title',)

class FaqPageAdmin(admin.ModelAdmin):
    inlines = [FaqItemInline,]

class MainPageAdmin(admin.ModelAdmin):
    inlines = [FeelTheDifferenceInline, WhyItSoPopularInline]

class AboutPageAdmin(admin.ModelAdmin):
	list_display = ('title',)

class HowToInstallPageAdmin(admin.ModelAdmin):
    inlines = [HowToInstallVideoInline, HowToInstallActionsInline]

class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('name','email','text','checked', 'pub_date')
    list_filter = ('checked','pub_date')

admin.site.register(MainPage,MainPageAdmin)
admin.site.register(AboutPage,AboutPageAdmin)
admin.site.register(HowToInstallPage,HowToInstallPageAdmin)
admin.site.register(FaqPage,FaqPageAdmin)
admin.site.register(ConstactPage,ConstactPageAdmin)
admin.site.register(SpecialOfferPage,SpecialOfferPageAdmin)
admin.site.register(Feedback,FeedbackAdmin)