# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings
from django.core.mail import EmailMessage
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _
from django.forms.formsets import formset_factory
from kits.shop.models import Order

class CartForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CartForm, self).__init__(*args, **kwargs)
        self.fields['delivery'].empty_label = None
        
    class Meta:
        model = Order
        widgets = {
            'delivery': forms.RadioSelect(),
        }








