# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'MainPage.feel_the_difference_block'
        db.delete_column(u'main_mainpage', 'feel_the_difference_block_id')

        # Adding field 'FeelTheDifference.feel_the_difference_block'
        db.add_column(u'main_feelthedifference', 'feel_the_difference_block',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['main.MainPage']),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'MainPage.feel_the_difference_block'
        db.add_column(u'main_mainpage', 'feel_the_difference_block',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['main.FeelTheDifference']),
                      keep_default=False)

        # Deleting field 'FeelTheDifference.feel_the_difference_block'
        db.delete_column(u'main_feelthedifference', 'feel_the_difference_block_id')


    models = {
        u'main.feelthedifference': {
            'Meta': {'object_name': 'FeelTheDifference'},
            'feel_the_difference_block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.MainPage']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'without_protection': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'main.mainpage': {
            'Meta': {'object_name': 'MainPage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        }
    }

    complete_apps = ['main']