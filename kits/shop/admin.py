from django.contrib import admin
from kits.shop.models import Brand, Model, Product, ProductImage, Delivery, Country, State

class StateInline(admin.TabularInline):
    model = State

class CountryAdmin(admin.ModelAdmin):
    list_display = ('name',)
    inlines = [StateInline,]

class DiscountAdmin(admin.ModelAdmin):
    list_display = ('code','discount')

class DeliveryAdmin(admin.ModelAdmin):
    list_display = ('title','price')

class ProductDeliveryAdmin(admin.ModelAdmin):
    list_display = ('price',)

class ModelInline(admin.TabularInline):
    prepopulated_fields = {"slug": ("title",)}
    model = Model

class BrandAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title',)
    prepopulated_fields = {"slug": ("title",)}
    inlines = [ModelInline,]

class ProductImageInline(admin.TabularInline):
    model = ProductImage

class ProductAdmin(admin.ModelAdmin):
    list_display = ('title','brand','model','price','hot_sale')
    search_fields = ('title','brand','model',)
    list_filter = ('hot_sale', 'brand')
    inlines = [ProductImageInline,]

admin.site.register(Country,CountryAdmin)
admin.site.register(Delivery,DeliveryAdmin)
admin.site.register(Product,ProductAdmin)
admin.site.register(Brand,BrandAdmin)
