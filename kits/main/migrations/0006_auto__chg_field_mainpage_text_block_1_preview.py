# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'MainPage.text_block_1_preview'
        db.alter_column(u'main_mainpage', 'text_block_1_preview', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True))

    def backwards(self, orm):

        # Changing field 'MainPage.text_block_1_preview'
        db.alter_column(u'main_mainpage', 'text_block_1_preview', self.gf('django.db.models.fields.CharField')(default=1, max_length=60))

    models = {
        u'main.feelthedifference': {
            'Meta': {'object_name': 'FeelTheDifference'},
            'feel_the_difference_block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.MainPage']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'without_protection': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'main.mainpage': {
            'Meta': {'object_name': 'MainPage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text_block_1_preview': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'text_block_1_text': ('django.db.models.fields.TextField', [], {}),
            'text_block_1_title': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        }
    }

    complete_apps = ['main']