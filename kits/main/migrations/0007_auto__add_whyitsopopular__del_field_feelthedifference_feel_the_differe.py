# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'WhyItSoPopular'
        db.create_table(u'main_whyitsopopular', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('reason_title', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('reason_text', self.gf('django.db.models.fields.TextField')()),
            ('page', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.MainPage'])),
        ))
        db.send_create_signal(u'main', ['WhyItSoPopular'])

        # Deleting field 'FeelTheDifference.feel_the_difference_block'
        db.delete_column(u'main_feelthedifference', 'feel_the_difference_block_id')

        # Adding field 'FeelTheDifference.page'
        db.add_column(u'main_feelthedifference', 'page',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['main.MainPage']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'WhyItSoPopular'
        db.delete_table(u'main_whyitsopopular')

        # Adding field 'FeelTheDifference.feel_the_difference_block'
        db.add_column(u'main_feelthedifference', 'feel_the_difference_block',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['main.MainPage']),
                      keep_default=False)

        # Deleting field 'FeelTheDifference.page'
        db.delete_column(u'main_feelthedifference', 'page_id')


    models = {
        u'main.feelthedifference': {
            'Meta': {'object_name': 'FeelTheDifference'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.MainPage']"}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'without_protection': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'main.mainpage': {
            'Meta': {'object_name': 'MainPage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text_block_1_preview': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'text_block_1_text': ('django.db.models.fields.TextField', [], {}),
            'text_block_1_title': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'main.whyitsopopular': {
            'Meta': {'object_name': 'WhyItSoPopular'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.MainPage']"}),
            'reason_text': ('django.db.models.fields.TextField', [], {}),
            'reason_title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        }
    }

    complete_apps = ['main']