# -*- coding: utf-8 -*-
from .. decorators import render_to, render_to_json
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext
from django.http import Http404,HttpResponse
from django.shortcuts import render_to_response, HttpResponseRedirect, redirect
from django.core.urlresolvers import reverse
from django.core.mail import send_mail, BadHeaderError
from django.core import serializers
from kits.main.models import MainPage, AboutPage, HowToInstallPage, FaqPage, ConstactPage, SpecialOfferPage
from kits.main.forms import FeedbackForm

@render_to('main/home.html')
def home(request):
	page = MainPage.objects.get(pk=1)
	return {
		'page':page
	}

@render_to('main/about.html')
def about(request):
	page = AboutPage.objects.get(pk=1)
	return {
		'page':page
	}

@render_to('main/how_to_install.html')
def how_to_install(request):
	page = HowToInstallPage.objects.get(pk=1)
	return {
		'page':page
	}

@render_to('main/faq.html')
def faq(request):
	page = FaqPage.objects.get(pk=1)
	return {
		'page':page
	}

@render_to('main/contacts.html')
def contacts(request):
	page = ConstactPage.objects.get(pk=1)
	return {
		'page':page
	}

@render_to('main/special_offer.html')
def special_offer(request):
	page = SpecialOfferPage.objects.get(pk=1)
	return {
		'page':page
	}

@csrf_exempt
def feedback(request):
	form = FeedbackForm(request.POST or None)
	if form.is_valid():
		cd = form.cleaned_data
		obj = form.save()
		if cd['send_mail_copy']==True:
			subject = u"Feedback copy message"
			from_email = cd['email']
			message =''
			message += u"Name: %s \n\n" % cd['name']
			message += u"E-mail: %s \n\n" % cd['email']
			message += "Message:\n"
			message += cd['text']
			try:
				send_mail(subject, message, settings.EMAIL_HOST_USER, [from_email,])
			except BadHeaderError:
				raise Http404
		return HttpResponse("Feedback successfully send")
	return HttpResponse("Failed")