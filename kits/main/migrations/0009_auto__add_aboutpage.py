# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AboutPage'
        db.create_table(u'main_aboutpage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('text_block_1_text', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'main', ['AboutPage'])


    def backwards(self, orm):
        # Deleting model 'AboutPage'
        db.delete_table(u'main_aboutpage')


    models = {
        u'main.aboutpage': {
            'Meta': {'object_name': 'AboutPage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text_block_1_text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'main.feelthedifference': {
            'Meta': {'object_name': 'FeelTheDifference'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.MainPage']"}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'without_protection': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'main.mainpage': {
            'Meta': {'object_name': 'MainPage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text_block_1_preview': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'text_block_1_text': ('django.db.models.fields.TextField', [], {}),
            'text_block_1_title': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'main.whyitsopopular': {
            'Meta': {'object_name': 'WhyItSoPopular'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.MainPage']"}),
            'reason_preview': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'reason_text': ('django.db.models.fields.TextField', [], {}),
            'reason_title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        }
    }

    complete_apps = ['main']