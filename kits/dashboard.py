# -*- coding: utf-8 -*-
"""
This file was generated with the customdashboard management command, it
contains the two classes for the main dashboard and app index dashboard.
You can customize these classes as you want.

To activate your index dashboard add the following to your settings.py::
    ADMIN_TOOLS_INDEX_DASHBOARD = 'kits.dashboard.CustomIndexDashboard'

And to activate the app index dashboard::
    ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'kits.dashboard.CustomAppIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name
from admin_tools.menu import items, Menu

class CustomMenu(Menu):
    def __init__(self, **kwargs):
        super(CustomMenu, self).__init__(**kwargs)
        self.children += [
            items.MenuItem('Overview', reverse('admin:index')),
        ]

class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for kits.
    """
    columns = 3
    
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        self.children.append(
            modules.ModelList(
                title = u'Administrator',
                models=(
                    'boats.accounts.models.User',
                    'django.contrib.auth.*',
                    'django.contrib.sites.*',
                    'kits.main.models.Feedback',
                ),
            )
        )

        self.children.append(
            modules.ModelList(
                title = u'Shop',
                models=(
                    'kits.shop.models.*',
                ),
            )
        )

        self.children.append(modules.LinkList(
            title = u'Pages',
            children=(
                ['Main page', '/admin/main/mainpage/1/'],
                ['How to install page', '/admin/main/howtoinstallpage/1/'],
                ['FAQ page', '/admin/main/faqpage/1/'],
                ['Contact page', '/admin/main/constactpage/1/'],
                ['About page', '/admin/main/aboutpage/1/'],
                ['Special offer page', '/admin/main/specialofferpage/1/'],
            )
        ))