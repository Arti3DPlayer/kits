# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'MainPage.title'
        db.add_column(u'main_mainpage', 'title',
                      self.gf('django.db.models.fields.CharField')(default=1, max_length=60),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'MainPage.title'
        db.delete_column(u'main_mainpage', 'title')


    models = {
        u'main.feelthedifference': {
            'Meta': {'object_name': 'FeelTheDifference'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'without_protection': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'main.mainpage': {
            'Meta': {'object_name': 'MainPage'},
            'feel_the_difference_block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.FeelTheDifference']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        }
    }

    complete_apps = ['main']