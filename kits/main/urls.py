# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings


urlpatterns = patterns('kits.main.views',
    url(r'^$', 'home', name='home'),
    url(r'^about/$', 'about', name='about'),
    url(r'^how_to_install/$', 'how_to_install', name='how_to_install'),
    url(r'^faq/$', 'faq', name='faq'),
    url(r'^contacts/$', 'contacts', name='contacts'),
    url(r'^special_offer/$', 'special_offer', name='special_offer'),
    url(r'^feedback/$', 'feedback', name='feedback'),
)