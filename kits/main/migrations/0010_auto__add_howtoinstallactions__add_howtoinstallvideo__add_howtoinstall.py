# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'HowToInstallActions'
        db.create_table(u'main_howtoinstallactions', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('action_preview', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True)),
            ('action_title', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('action_text', self.gf('django.db.models.fields.TextField')()),
            ('page', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.MainPage'])),
        ))
        db.send_create_signal(u'main', ['HowToInstallActions'])

        # Adding model 'HowToInstallVideo'
        db.create_table(u'main_howtoinstallvideo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('step', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('video_title', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('video', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True)),
            ('video_description', self.gf('django.db.models.fields.TextField')()),
            ('page', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.HowToInstallPage'])),
        ))
        db.send_create_signal(u'main', ['HowToInstallVideo'])

        # Adding model 'HowToInstallPage'
        db.create_table(u'main_howtoinstallpage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('text_block_1_text', self.gf('redactor.fields.RedactorField')()),
        ))
        db.send_create_signal(u'main', ['HowToInstallPage'])


        # Changing field 'AboutPage.text_block_1_text'
        db.alter_column(u'main_aboutpage', 'text_block_1_text', self.gf('redactor.fields.RedactorField')())

    def backwards(self, orm):
        # Deleting model 'HowToInstallActions'
        db.delete_table(u'main_howtoinstallactions')

        # Deleting model 'HowToInstallVideo'
        db.delete_table(u'main_howtoinstallvideo')

        # Deleting model 'HowToInstallPage'
        db.delete_table(u'main_howtoinstallpage')


        # Changing field 'AboutPage.text_block_1_text'
        db.alter_column(u'main_aboutpage', 'text_block_1_text', self.gf('django.db.models.fields.TextField')())

    models = {
        u'main.aboutpage': {
            'Meta': {'object_name': 'AboutPage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text_block_1_text': ('redactor.fields.RedactorField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'main.feelthedifference': {
            'Meta': {'object_name': 'FeelTheDifference'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.MainPage']"}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'without_protection': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'main.howtoinstallactions': {
            'Meta': {'object_name': 'HowToInstallActions'},
            'action_preview': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'action_text': ('django.db.models.fields.TextField', [], {}),
            'action_title': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.MainPage']"})
        },
        u'main.howtoinstallpage': {
            'Meta': {'object_name': 'HowToInstallPage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text_block_1_text': ('redactor.fields.RedactorField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'main.howtoinstallvideo': {
            'Meta': {'object_name': 'HowToInstallVideo'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.HowToInstallPage']"}),
            'step': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'video': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'}),
            'video_description': ('django.db.models.fields.TextField', [], {}),
            'video_title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'main.mainpage': {
            'Meta': {'object_name': 'MainPage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text_block_1_preview': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'text_block_1_text': ('django.db.models.fields.TextField', [], {}),
            'text_block_1_title': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'main.whyitsopopular': {
            'Meta': {'object_name': 'WhyItSoPopular'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.MainPage']"}),
            'reason_preview': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'reason_text': ('django.db.models.fields.TextField', [], {}),
            'reason_title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        }
    }

    complete_apps = ['main']