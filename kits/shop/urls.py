# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings


urlpatterns = patterns('kits.shop.views',
    url(r'^list/(?P<brand>[\w\-]+)/(?P<model>[\w\-]+)/$', 'car_list', name='car_list'),
    url(r'^sales/(?P<product>[\w\-]+)/$', 'hot_product', name='hot_product'),

    url(r'^cart/add/$', 'cart_add', name='cart_add'),
    url(r'^cart/delete/$', 'cart_delete', name='cart_delete'),
    url(r'^cart/show/$', 'cart_show', name='cart_show'),
    url(r'^cart/details/$', 'cart_details', name='cart_details'),
    url(r'^cart/refresh/$', 'cart_refresh', name='cart_refresh'),
    url(r'^cart/states/$', 'cart_states', name='cart_states'),

    url(r'^cart/$', 'cart', name='cart'),

    url(r'^admin/product/model/ajax/$', 'admin_model_filter', name='admin_model_filter'),

)