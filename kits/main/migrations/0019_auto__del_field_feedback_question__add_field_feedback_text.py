# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Feedback.question'
        db.delete_column(u'main_feedback', 'question')

        # Adding field 'Feedback.text'
        db.add_column(u'main_feedback', 'text',
                      self.gf('django.db.models.fields.CharField')(default=1, max_length=30),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Feedback.question'
        db.add_column(u'main_feedback', 'question',
                      self.gf('django.db.models.fields.CharField')(default=1, max_length=30),
                      keep_default=False)

        # Deleting field 'Feedback.text'
        db.delete_column(u'main_feedback', 'text')


    models = {
        u'main.aboutpage': {
            'Meta': {'object_name': 'AboutPage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text_block_1_text': ('redactor.fields.RedactorField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'main.constactpage': {
            'Meta': {'object_name': 'ConstactPage'},
            'adress': ('redactor.fields.RedactorField', [], {}),
            'email': ('redactor.fields.RedactorField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('redactor.fields.RedactorField', [], {}),
            'text': ('redactor.fields.RedactorField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'we_work': ('redactor.fields.RedactorField', [], {})
        },
        u'main.faqitem': {
            'Meta': {'object_name': 'FaqItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.FaqPage']"}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'main.faqpage': {
            'Meta': {'object_name': 'FaqPage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text_block_1_text': ('redactor.fields.RedactorField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'main.feedback': {
            'Meta': {'object_name': 'Feedback'},
            'email': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'main.feelthedifference': {
            'Meta': {'object_name': 'FeelTheDifference'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.MainPage']"}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'without_protection': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'main.howtoinstallactions': {
            'Meta': {'object_name': 'HowToInstallActions'},
            'action_preview': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'action_text': ('django.db.models.fields.TextField', [], {}),
            'action_title': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.HowToInstallPage']"})
        },
        u'main.howtoinstallpage': {
            'Meta': {'object_name': 'HowToInstallPage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text_block_1_text': ('redactor.fields.RedactorField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'main.howtoinstallvideo': {
            'Meta': {'object_name': 'HowToInstallVideo'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.HowToInstallPage']"}),
            'step': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'video': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'}),
            'video_description': ('django.db.models.fields.TextField', [], {}),
            'video_title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'main.mainpage': {
            'Meta': {'object_name': 'MainPage'},
            'how_it_works_text': ('redactor.fields.RedactorField', [], {}),
            'how_it_works_title': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'how_it_works_video': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text_block_1_preview': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'text_block_1_text': ('redactor.fields.RedactorField', [], {}),
            'text_block_1_title': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'main.specialofferitem': {
            'Meta': {'object_name': 'SpecialOfferItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.SpecialOfferPage']"}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'main.specialofferpage': {
            'Meta': {'object_name': 'SpecialOfferPage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text_block_1_text': ('redactor.fields.RedactorField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'main.whyitsopopular': {
            'Meta': {'object_name': 'WhyItSoPopular'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.MainPage']"}),
            'reason_preview': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'reason_text': ('django.db.models.fields.TextField', [], {}),
            'reason_title': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        }
    }

    complete_apps = ['main']