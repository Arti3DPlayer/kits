# -*- coding: utf-8 -*-
from django.db import models
from redactor.fields import RedactorField

class Delivery(models.Model):
    title   = models.CharField(max_length=30, verbose_name=u'Название компании')
    url     = models.URLField(max_length=200, blank=True, verbose_name=u'Company url')
    price   = models.IntegerField(default=0, verbose_name=u'Цена')

    class Meta:
        verbose_name = u'shipping company'
        verbose_name_plural = u'Shipping companies'

    def __unicode__(self):
        return self.title


class Brand(models.Model):
    title = models.CharField(max_length=30, verbose_name=u'Brand name')
    slug = models.SlugField(max_length=30, verbose_name=u'Slug')
    class Meta:
        verbose_name = u'Brand'
        verbose_name_plural = u'Brands'

    @property
    def get_models(self):
        return Model.objects.filter(brand=self)

    def __unicode__(self):
        return self.title

class Model(models.Model):
    title = models.CharField(max_length=30, verbose_name=u'Model name')
    slug = models.SlugField(max_length=30, verbose_name=u'Slug')
    brand = models.ForeignKey(Brand, verbose_name=u'Brand')

    class Meta:
        verbose_name = u'Model'
        verbose_name_plural = u'Models'

    def __unicode__(self):
        return self.title

class Product(models.Model):
    title = models.CharField(max_length=30, verbose_name=u'Title')
    brand = models.ForeignKey(Brand, verbose_name= u'Brand',)
    model = models.ForeignKey(Model, verbose_name= u'Model')
    short_description = models.CharField(max_length=60, verbose_name= u'Short description')
    text = RedactorField(verbose_name=u'Text',)
    pub_date = models.DateTimeField(auto_now=True, auto_now_add=True, verbose_name=u'Pub date')
    price = models.DecimalField(default=0, verbose_name=u'Price', max_digits=8,decimal_places=2)
    preview = models.ImageField(verbose_name=u'Preview image', upload_to='products/', null = True)
    hot_sale=models.BooleanField(verbose_name=u'Hot sale')

    def get_product_images(self):
        return ProductImage.objects.filter(product=self)

    class Meta:
        verbose_name = u'Product'
        verbose_name_plural = u'Products'

    def __unicode__(self):
        return self.title


class ProductImage(models.Model):
    image = models.ImageField(verbose_name=u'Изображение', upload_to='products/', null = True, blank = True)
    product = models.ForeignKey(Product)

    class Meta:
        verbose_name = u'Image'
        verbose_name_plural = u'Imgaes'

    def __unicode__(self):
        return self.image

class Discount(models.Model):
    code =  models.CharField(max_length=30)
    discount =  models.IntegerField(default=0)


class Country(models.Model):
    name =  models.CharField(max_length=30)

    class Meta:
        verbose_name = u'Country'
        verbose_name_plural = u'Countries'

    def __unicode__(self):
        return self.name

class State(models.Model):
    name =  models.CharField(max_length=30)
    country = models.ForeignKey(Country)

    class Meta:
        verbose_name = u'State'
        verbose_name_plural = u'States'

    def __unicode__(self):
        return self.name

PAYMENT_CHOICES = (('1', 'Credit Card'),
                    ('2', 'Paypall'))


class Order(models.Model):
    delivery    = models.ForeignKey(Delivery, verbose_name= u'Delivery')
    discount    = models.CharField(max_length=30, verbose_name=u'Discount')
    payment     = models.CharField(max_length=20, verbose_name=u'Payment', choices=PAYMENT_CHOICES)
    first_name  = models.CharField(max_length=20, verbose_name=u'First name')
    middle_name = models.CharField(max_length=20, verbose_name=u'Middle name')
    last_name   = models.CharField(max_length=20, verbose_name=u'Last name')
    email       = models.CharField(max_length=20, verbose_name=u'Email')
    phone       = models.CharField(max_length=20, verbose_name=u'Telephone number')
    adress1     = models.CharField(max_length=20, verbose_name=u'Adress1')
    adress2     = models.CharField(max_length=20, blank=True, verbose_name=u'Adress2')
    city        = models.CharField(max_length=20, verbose_name=u'City')
    country     = models.ForeignKey(Country, verbose_name= u'Country')
    state       = models.ForeignKey(State, verbose_name= u'State')
    zipcode     = models.CharField(max_length=20, verbose_name=u'Zip Code')