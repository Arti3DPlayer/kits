# -*- coding: utf-8 -*-
from django import forms
from kits.main.models import Feedback

class FeedbackForm(forms.ModelForm):
	send_mail_copy = forms.BooleanField(required=False,initial=False,label='Send copy on my e-mail')
	class Meta:
		model = Feedback
		exclude = ('checked',)