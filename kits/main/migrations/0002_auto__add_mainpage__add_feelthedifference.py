# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MainPage'
        db.create_table(u'main_mainpage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('feel_the_difference_block', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.FeelTheDifference'])),
        ))
        db.send_create_signal(u'main', ['MainPage'])

        # Adding model 'FeelTheDifference'
        db.create_table(u'main_feelthedifference', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('without_protection', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'main', ['FeelTheDifference'])


    def backwards(self, orm):
        # Deleting model 'MainPage'
        db.delete_table(u'main_mainpage')

        # Deleting model 'FeelTheDifference'
        db.delete_table(u'main_feelthedifference')


    models = {
        u'main.feelthedifference': {
            'Meta': {'object_name': 'FeelTheDifference'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'without_protection': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'main.mainpage': {
            'Meta': {'object_name': 'MainPage'},
            'feel_the_difference_block': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.FeelTheDifference']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['main']