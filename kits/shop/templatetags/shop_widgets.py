# -*- coding: utf-8 -*-
from django.conf import settings
from django import template
from django.utils.translation import ugettext_lazy as _
from django.contrib.flatpages.models import FlatPage
from django.db.models import Q, F, Avg, Count

from kits.shop.models import Brand, Product

register = template.Library()

@register.inclusion_tag('shop/inclusion/brand_list.html')
def brands_list_tag():
	b = Brand.objects.order_by('title')
	return {
        'brands': b
    }

@register.inclusion_tag('shop/inclusion/hot_sales_list.html')
def hot_sales_list_tag():
	p = Product.objects.filter(hot_sale=True).order_by('-pub_date')
	return {
        'hot_products': p
    }