# -*- coding: utf-8 -*-
from django.conf import settings
from django import template
from django.utils.translation import ugettext_lazy as _
from django.contrib.flatpages.models import FlatPage
from django.db.models import Q, F, Avg, Count

from kits.main.forms import FeedbackForm

register = template.Library()

@register.inclusion_tag('main/inclusion/feedback_form.html')
def feedback_form_tag():
    return {
        'feedback_form': FeedbackForm()
    }