function add_to_cart(id){
  $.ajax({
    url: '/shop/cart/add/?id='+id,
    type : "GET",
    dataType: "json",
    beforeSend: function () {
      $('#cart').html('Загрузка...');
    },
    success: function (data) {
      refresh_cart(data);
      alert('Item added to cart.');
    }
  });
}

function refresh_cart(data){//refresh cart at the top
  text = '';
  if(data['count'] == 0){
    $('#cart-href').attr('href','#');
  }
  else {
    $('#cart-href').attr('href','/shop/cart/');
  }
  text+='<p>Shopping Cart: <span>$'+data['price']+'</span></p>'
  text+='<p>Now in your cart '+data['count']+' items</p>'
  $('#cart').html(text);
}


function cart_load() {
  $.ajax({
  url: '/shop/cart/show/',
  type : "GET",
  dataType: "json",
  beforeSend: function () {
    $('#cart').html('Загрузка...');
  },
  success: function (data) {
    refresh_cart(data);
  }
});
}

cart_load();//refreshing cart at loading page


function delete_item (item_id) {
  $.ajax({
    url: '/shop/cart/delete/?id='+item_id,
    type : "GET",
    dataType: "json",
    success: function () {
      alert('Item removed from cart.');
    },
    complete: function () {
      cart_details_load();
      cart_load();
    }
  });
}


//returns json data that contains full information of item in cart
function cart_details_load () {
  var delivery = $('input[name=delivery]:checked').val()//get delivery value
  var discount = $('#id_discount').val()//get discount value
  if(!delivery){delivery =0;};
  if (!discount){discount=0;};
  $.ajax({
    url: '/shop/cart/details/?delivery='+delivery+'&&discount='+discount,
    type : "GET",
    dataType: "json",
    success: function (data) {
        var table = ''
        table+='<tr>' +
               '<td>Del</td>'+
               '<td class="item">Item</td>'+
               '<td class="carModel">Car model</td>'+
               '<td class="quontity">Quontity</td>'+
               '<td>Price</td>'+
               '</tr>';
        for (i=0; i<data['cart'].length; i++) {
            table +='<tr>' +
                    '<td><a href="#" onclick="delete_item(' + data['cart'][i]['id'] + ')"><i class="icon-remove"></i></a></td>'+
                    '<td class="item">' + data['cart'][i]['title'] + '</td>'+
                    '<td class="carModel">' + data['cart'][i]['brand'] + ' ' + data['cart'][i]['model'] +'</td>' +
                    '<td><input type="text" onchange="refresh_summary(' + data['cart'][i]['id'] + ',this)" name="' + data['cart'][i]['id'] + '" value="' + data['cart'][i]['count'] + '" class="input-mini"></div></td>' +
                    //'<td class="quontity">' + data['cart'][i]['count'] + '</td>'+
                    '<td><span>' + (data['cart'][i]['price']*data['cart'][i]['count']).toFixed(2) + '</span>$</td>' +
                    '</tr>';
        }
        $('#products-table tbody').text('');
        $('#products-table tbody').append(table);
        $('#total_item_cost').text(data['price']);
        $('#total_item_discount').text((data['discount']).toFixed(2));
        $('#total_shipping').text((data['delivery']).toFixed(2));
        $('#total_cost').text((data['price']+data['delivery']-data['discount']).toFixed(2));
        console.log('Список товаров загружен...');
    }
  });
}

//refreshing info when change count of item in cart page
function refresh_summary(item_id,item) {
  var count = parseInt($(item).val());//get item count from field
  if( Math.floor(count) == count && $.isNumeric(count)) {//check if enter symbol number
    if(count<1||count>99) {//check if entered number in current range
      count=1;
      alert('Введите число от 1 до 99');
    }
  }
  else {
    alert('Введите число от 1 до 99');
    count=1;
  }
  $.ajax({
    url: '/shop/cart/refresh/?id='+item_id+'&&count='+count,
    type : "GET",
    dataType: "json",
    success: function () {
      console.log('Количество товара изменено...');
      cart_details_load();
      cart_load();
    }
  });
}