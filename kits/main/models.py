from django.db import models
from redactor.fields import RedactorField


class MainPage(models.Model):
	title = models.CharField(max_length=60,verbose_name=u'Page title')
	text_block_1_title = models.CharField(max_length=60,verbose_name=u'Block1 title')
	text_block_1_preview = models.ImageField(upload_to='mainpage/', null = True, blank = True, verbose_name=u'Block1 preview image')
	text_block_1_text = RedactorField(verbose_name=u'Block1 text')
	how_it_works_title = models.CharField(max_length=60,verbose_name=u'Block2 title')
	how_it_works_video = models.FileField(upload_to='mainpage/', verbose_name=u'Block2 VideoFile', null=True)
	how_it_works_text = RedactorField(verbose_name=u'Block2 text')

	@property
	def get_bad_differences(self):
		return FeelTheDifference.objects.filter(without_protection=True)

	@property
	def get_good_differences(self):
		return FeelTheDifference.objects.filter(without_protection=False)

	@property
	def get_why_it_so_popular(self):
		return WhyItSoPopular.objects.all()


class FeelTheDifference(models.Model):
	text = models.TextField(verbose_name=u'Text')
	without_protection = models.BooleanField(verbose_name=u'Without protection')
	page = models.ForeignKey(MainPage)


class WhyItSoPopular(models.Model):
	reason_preview = models.ImageField(upload_to='mainpage/', null = True, verbose_name=u'Reason preview image')
	reason_title = models.CharField(max_length=60,verbose_name=u'Reason title')
	reason_text = models.TextField(verbose_name=u'Reason text')
	page = models.ForeignKey(MainPage)


class AboutPage(models.Model):
	title = models.CharField(max_length=60,verbose_name=u'Page title')
	text_block_1_text = RedactorField(verbose_name=u'Block text')

class HowToInstallPage(models.Model):
	title = models.CharField(max_length=60,verbose_name=u'Page title')
	text_block_1_text = RedactorField(verbose_name=u'Block text')

	@property
	def get_video_steps(self):
		return HowToInstallVideo.objects.order_by('step')

	@property
	def get_actions(self):
		return HowToInstallActions.objects.all()


class FaqPage(models.Model):
	title = models.CharField(max_length=60,verbose_name=u'Page title')
	text_block_1_text = RedactorField(verbose_name=u'Block text')

	@property
	def get_faq_item(self):
		return FaqItem.objects.all()

class FaqItem(models.Model):
	title = models.CharField(max_length=200,verbose_name=u'Item title')
	text = models.TextField(verbose_name=u'Description')
	page = models.ForeignKey(FaqPage)

class SpecialOfferPage(models.Model):
	title = models.CharField(max_length=60,verbose_name=u'Page title')
	text_block_1_text = RedactorField(verbose_name=u'Block text')

	@property
	def get_faq_item(self):
		return SpecialOfferItem.objects.all()

class SpecialOfferItem(models.Model):
	title = models.CharField(max_length=200,verbose_name=u'Item title')
	text = models.TextField(verbose_name=u'Description')
	page = models.ForeignKey(SpecialOfferPage)


class ConstactPage(models.Model):
	title = models.CharField(max_length=60,verbose_name=u'Page title')
	text = RedactorField(verbose_name=u'Block text')
	email = RedactorField(verbose_name=u'Email')
	adress = RedactorField(verbose_name=u'Adress')
	phone = RedactorField(verbose_name=u'Phone')
	we_work = RedactorField(verbose_name=u'We work')

class HowToInstallVideo(models.Model):
	step = models.IntegerField(default=1, verbose_name=u'Step number')
	video_title = models.CharField(max_length=60,verbose_name=u'Video title')
	video = models.FileField(upload_to='howtoinstallpage', verbose_name=u'File', null=True)
	video_description = models.TextField(verbose_name=u'Video description')
	page = models.ForeignKey(HowToInstallPage)


class HowToInstallActions(models.Model):
	action_preview = models.ImageField(upload_to='mainpage/', null = True, verbose_name=u'Action preview image')
	action_title = models.CharField(max_length=60,verbose_name=u'Action title')
	action_text = models.TextField(verbose_name=u'Action text')
	page = models.ForeignKey(HowToInstallPage)


class Feedback(models.Model):
	name = models.CharField(max_length=20,verbose_name=u'Name')
	email = models.CharField(max_length=30,verbose_name=u'Email')
	text = models.TextField(verbose_name=u'Question')
	checked = models.BooleanField(verbose_name=u'Checked')
	pub_date = models.DateTimeField(auto_now=True, auto_now_add=True, verbose_name=u'Pub date')