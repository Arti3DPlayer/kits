# -*- coding: utf-8 -*-
from .. decorators import render_to, render_to_json
from django.views.decorators.csrf import csrf_exempt

from django.template import RequestContext
from django.http import Http404,HttpResponse
from django.shortcuts import render_to_response, HttpResponseRedirect, redirect
from django.core.urlresolvers import reverse

from django.core import serializers

from kits.shop.models import Brand, Model, Product, Delivery, Discount, State
from kits.shop.forms import CartForm

from django.conf import settings

@render_to('shop/car_list.html')
def car_list(request,brand,model):
    products = Product.objects.filter(brand__slug=brand,model__slug=model).order_by('-pub_date')
    return {
        'products': products,
    }

@render_to('shop/hot_product.html')
def hot_product(request,product):
    product = Product.objects.get(id=product)
    return {
        'product': product,
    }

@render_to_json
def admin_model_filter(request):
    """
    Связные списки марка-модель в админке
    """
    data = {}
    if request.GET.get('brand'):
        brand_id = request.GET.get('brand')
        data = Model.objects.filter(brand=brand_id)
    data=serializers.serialize('json',data)
    return data



@render_to('shop/cart.html')
def cart(request):
    d = Delivery.objects.all()
    f = CartForm()

    if request.method == 'POST':
        f = CartForm(request.POST)
        if f.is_valid():
            print "VALID"
    return {
        'delivery': d,
        'CartForm': f,
    }


@csrf_exempt
@render_to_json
def cart_details(request):
    cart = request.session['cart']

    try:
        delivery = Delivery.objects.get(id=int(request.GET.get('delivery'))).price
    except (Delivery.DoesNotExist, ValueError):
        delivery = 0
    try:
        discount = Discount.objects.get(code=request.GET.get('discount')).discount
    except Discount.DoesNotExist:
        discount = 0
    res = {}
    res['count'] = cart['count']
    res['price'] = cart['price']
    res['delivery'] = delivery
    res['discount'] = discount
    res['cart'] = []

    for key in cart['cart'].keys():
        product = Product.objects.get(id = key)
        res['cart'].append({
            'id' : product.id, 
            'title' : product.title,
            'brand': str(product.brand),
            'model': str(product.model), 
            'price' : product.price, 
            'count' : cart['cart'][key]
            })
    return res    

@csrf_exempt
@render_to_json
def cart_add(request):
    cart = request.session['cart']
    if request.GET:
        id = request.GET.get('id')
        product = Product.objects.get(id = id)
        if cart['cart'].has_key(product.id):
            cart['cart'][product.id] += 1
        else:
            cart['cart'][product.id] = 1
        cart['count'] += 1
        cart['price'] += product.price
        request.session['cart'] = cart
        return {
            'count' : request.session['cart']['count'], 
            'price' : request.session['cart']['price']
        }
    else:
        return {}

@csrf_exempt
@render_to_json
def cart_show(request):
    if request.session.has_key('cart'):
        cart = request.session['cart']
        count = cart['count']
        price = cart['price']
        return {
            'count' : count, 
            'price' : price
        }
    else:
        request.session['cart'] = {
            'count' : 0,
            'price' : 0,
            'cart' : {}
        }
        return request.session['cart']


@csrf_exempt
@render_to_json
def cart_delete(request):
    cart = request.session['cart']
    if request.GET:
        id = request.GET.get('id')
        try:
            id = int(id)
            if cart['cart'].has_key(id):
                cart['count'] -= cart['cart'][id]
                cart['price'] -= cart['cart'][id] * Product.objects.get(id = id).price
                del cart['cart'][id]
        except:
            print "Item id not correctly"
    request.session['cart'] = cart


@csrf_exempt
@render_to_json
def cart_refresh(request):
    cart = request.session['cart']
    if request.GET:
        id = request.GET.get('id')
        count = request.GET.get('count')
        try:
            id = int(id)
            count = int(count)
        except:
            print "Id error"

        if cart['cart'].has_key(id):
            cart['count']-= cart['cart'][id]
            cart['price'] -= cart['cart'][id] * Product.objects.get(id = id).price
            cart['cart'][id] = count
            cart['count']+= count
            cart['price']+= count*Product.objects.get(id = id).price
    request.session['cart'] = cart
        


@csrf_exempt
@render_to_json
def cart_states(request):
    data = {}
    try:
        country_id = int(request.GET.get('country'))
    except ValueError:
        country_id = 0
    data = State.objects.filter(country=country_id)
    data=serializers.serialize('json',data)
    return data